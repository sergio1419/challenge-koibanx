const mongoose = require("mongoose");

const url = `mongodb://${process.env.HOST_DB}/${process.env.NAME_DB}`;
const opciones = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

mongoose.connect(url, opciones)
    .then(db => {
        // console.log("Base de datos conectada");
    }).catch(err => {
        console.log(err);
    });

module.exports = mongoose;

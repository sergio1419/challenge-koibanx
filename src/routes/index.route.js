const { Router } = require("express");
const router = Router();

const uploadFIleCtr = require("../controllers/upload.controller");
const jwtCtr = require("../controllers/jwt.controller");
const uploadSvc = require("../services/multer.service");
const jwtSvc = require("../services/jwt.service");

router.get("/getToken", jwtCtr.getToken);
router.post("/upload", jwtSvc.verifyJWT, uploadSvc.single("docExcel"), uploadFIleCtr.upload);
router.get("/getUpload", jwtSvc.verifyJWT, uploadFIleCtr.getUpload);
router.get("/getErrorsUpload", jwtSvc.verifyJWT, uploadFIleCtr.getErrorsUpload);

module.exports = router

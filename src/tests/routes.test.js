const mongoose = require('mongoose');
const server = require("../../index");
const request = require("supertest");
const FormData = require('form-data');
const fs = require('fs');

var token = "";
var loadId = 0;

describe("Get / getToken", () => {
    test("Should respond with a http 200 code", async () => {
        const response = await request(server).get("/getToken")
        expect(response.status).toBe(200);
    });

    test("Should respond with a json", async () => {
        const response = await request(server).get("/getToken")
        expect(response.body).toHaveProperty("status", "done");
        expect(response.body).toHaveProperty("token");
        token = response.body.token;
        /*  The value that the "token" property must have is not checked because for 
            each request this value changes, o the test would never pass */
    });
});

describe("Post / upload", () => {
    test("Should respond with a http 200 code next to the loadId", async () => {
        const form = new FormData();
        form.append('mappingFormat', '[{"name": "id","type": "number"},{"name": "name","type":"string"},{"name": "lastName","type":"string"},{"name": "email","type": "string"},{"name": "age","type":"number"}]');
        form.append('docExcel', fs.createReadStream(__dirname + '/tests/datatTest.xlsx'));

        const response = await request(server).post("/upload")
                                    .set('Content-Type', 'multipart/form-data')
                                    .set("authorization", `Bearer ${token}`)
                                    .set("Accept", "text/event-stream")
                                    .field('mappingFormat', '[{"name": "id","type": "number"},{"name": "name","type":"string"},{"name": "lastName","type":"string"},{"name": "email","type": "string"},{"name": "age","type":"number"}]')
                                    .attach('docExcel', __dirname + '/datatTest.xlsx');
        expect(response.status).toBe(200);
        const formattedString = `{${response.text.split("\n")[1]}}`.replace('data', '"data"');
        loadId = parseInt(JSON.parse(formattedString).data.result.loadId);
    });
});

describe("Get / getUpload", () => {
    test("Should respond with a http 200 next to the object for the loadId sent", async () => {
        const response = await request(server).get("/getUpload")
                                    .send({"loadId" : loadId, "queryType" : 1})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}`);
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty("status", "done");
        expect(response.body).toHaveProperty("mesaage", "Results according to your query");
        expect(response.body.result.data).toHaveProperty("_id", loadId.toString());
    });

    test("Should respond with a http 404 next to the object for the loadId sent", async () => {
        const response = await request(server).get("/getUpload")
                                    .send({"loadId" : 1678341769787, "queryType" : 1})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}`);
        expect(response.status).toBe(404);
        expect(response.body).toHaveProperty("status", "failed");
        expect(response.body).toHaveProperty("mesaage", "Results according to your query");
    });

    test("Should respond with a http 200 code next to the object(s) for queryType = 0", async () => {
        const response = await request(server).get("/getUpload")
                                    .send({"loadId" : "", "queryType" : 0})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}`);
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty("status", "done");
        expect(response.body).toHaveProperty("mesaage", "Results according to your query");
    });
});

describe("Get / getErrorsUpload", () => {
    test("Should respond with a http 200 code with no errors for the loadId sent", async () => {
        const response = await request(server).get("/getErrorsUpload")
                                    .send({"loadId" : loadId, "page" : 1, "limit" : 3})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}`);
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty("status", "failed");
        expect(response.body).toHaveProperty("mesaage", "Conversion of the indicated file did not generate any errors");
        /*  If reading or converting the file generates errors, you will receive a 200 status code because the 
            loadId was found but with a "done" status and an "Errors found for the provided query" message. */
    });

    test("Should respond with a http 404 code next to the object for the loadId sent", async () => {
        const response = await request(server).get("/getErrorsUpload")
                                    .send({"loadId" : 1678341769787, "page" : 1, "limit" : 3})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}`);
        expect(response.status).toBe(404);
        expect(response.body).toHaveProperty("status", "failed");
        expect(response.body).toHaveProperty("mesaage", "No results found for the provided loadId");
    });

    test("Should respond with a http 401 code", async () => {
        const response = await request(server).get("/getErrorsUpload")
                                    .send({"loadId" : loadId, "page" : 1, "limit" : 3})
                                    .set("Content-Type", "application/json")
                                    .set("authorization", `Bearer ${token}1`);
        expect(response.status).toBe(401);
    });
});

afterAll(async () => {
    await mongoose.disconnect();
    await new Promise(resolve => server.close(resolve));
});

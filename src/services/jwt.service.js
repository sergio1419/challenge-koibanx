const jwt = require("jsonwebtoken");

const generateJWT = () => {
    const token = jwt.sign({}, process.env.SECRET_KEY, { expiresIn : "1h"});
    return token;
}

const verifyJWT = (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];
    
    if (!token) throw { status: 401, message: "This request does not have the required authorization" };
  
    try {
        jwt.verify(token, process.env.SECRET_KEY);
        next();
    } catch (err) {
        throw { status: 401, message: err.message };
    }
}

module.exports = {
    generateJWT,
    verifyJWT
}

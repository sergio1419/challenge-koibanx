const multer = require("multer");
const mimeTypes = require("mime-types");

const fileName = (req, file, cb) => {
    const nameFile = Date.now();
    cb("", `${nameFile}.${mimeTypes.extension(file.mimetype)}`);
    req.body["nameFile"] = nameFile;
}

const storage = multer.diskStorage({
    destination : "./uploads",
    filename: fileName
});

const xlsxFilter = (req, file, cb) => {
    if (file.mimetype === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        cb(null, true);
    } else {
        const error = new Error("The file you are trying to upload does not have the required xlsx extension");
        error.status = 415;
        cb(error, false);
    }
}

const upload = multer({
    storage : storage,
    fileFilter : xlsxFilter
});

module.exports = upload;

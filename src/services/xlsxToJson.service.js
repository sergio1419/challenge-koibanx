const XLSX = require('xlsx');
const jsonFile = require('jsonfile');
const { mapJson } = require("../utils/index.utils");
const UploadModel = require("../models/upload.model");

const xlsxToJson = (nameXlsx, mappingFormat) => {
    updateDoc(nameXlsx, "", 1);

    const workbook = XLSX.readFile(`./uploads/${nameXlsx}.xlsx`);
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const jsonData = XLSX.utils.sheet_to_json(worksheet);

    const numberPairs = Object.keys(jsonData[0]).length;
    if (numberPairs != mappingFormat.length) throw { status: 500, message: "The mapping format is not valid for the excel provided" };

    const { mappedJson, fileErrors } = mapJson(jsonData, numberPairs, mappingFormat);
    updateDoc(nameXlsx, fileErrors, 2);

    jsonFile.writeFile(`./jsons/${nameXlsx}.json`, mappedJson, (err) => {
        if (err) throw { status: 500, message: err.message };
    });
}

async function updateDoc (idDoc, fileErrors, key) {
    if (key == 1) {
        await UploadModel.updateOne({ _id : idDoc }, { status : "processing" });
    } else {
        await UploadModel.updateOne({ _id : idDoc }, { fileErrors : fileErrors });
    }
}

module.exports = {
    xlsxToJson,
}

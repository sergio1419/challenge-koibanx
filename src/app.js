require('dotenv').config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.set("port", process.env.PORT || 4000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require("./routes/index.route"));

app.use((err, req, res, next) => {
    if (err.status == undefined) {
        res.status(500).json({
            status  : false,
            message : "Internal Server Error"
        });
    } else {
        res.status(err.status).json({
            status  : false,
            message : err.message
        });
    }
});

module.exports = app;

const fs = require('fs');
const { xlsxToJson } = require("../services/xlsxToJson.service");
const { removeFile } = require("../utils/index.utils");
require("../config/connectionDB.config");
const UploadModel = require("../models/upload.model");

const upload = async (req, res) => {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });

    const { nameFile, mappingFormat } = req.body;

    const uploadModel = new UploadModel();

    try {
        res.write('event: status\n');
        res.write(`data: ${JSON.stringify({
            status: 'pending',
            message : "File upload completed",
            result  : {
                loadId : nameFile
            }
        })}\n\n`);
        
        uploadModel._id = nameFile;
        uploadModel.status = "pending";
        uploadModel.mappingFormat = JSON.parse(mappingFormat);
        await uploadModel.save();

        res.write('event: status\n');
        res.write(`data: ${JSON.stringify({
            status: 'processing',
            message : "The file is being converted",
            result  : {
                loadId : nameFile
            }
        })}\n\n`);
        
        updateDoc(nameFile, "processing");

        xlsxToJson(nameFile, JSON.parse(mappingFormat));

        const json = `./jsons/${nameFile}.json`;

        fs.readFile(json, (err, contJson) => {
            if (err) throw { status: 500, message: err.message };
            
            updateDoc(nameFile, "done");

            res.write('event: status\n');
            res.write(`data: ${JSON.stringify({
                status  : "done",
                message : "File converted successfully",
                result  : {
                    loadId : nameFile,
                    data   : JSON.parse(contJson)
                }
            })}\n\n`);

            res.end();
        });
    } catch (error) {
        const xlsx = `./uploads/${nameFile}.xlsx`;
        const json = `./jsons/${nameFile}.json`;

        if (fs.existsSync(xlsx)) {
            removeFile(xlsx)
        }

        if (fs.existsSync(json)) {
            removeFile(json)
        }
        
        await UploadModel.deleteOne({ _id : nameFile });
        
        res.write('event: status\n');
        res.write(`data: ${JSON.stringify({
                status  : "failed",
                message : "Error when uploading or converting the file",
                result  : {}
        })}\n\n`);

        res.end();
    }
}

async function updateDoc (nameXlsx, status) {
    await UploadModel.updateOne({ _id : nameXlsx }, { status : status });
}

const getUpload = async (req, res) => {
    const { loadId, queryType } = req.body;
    var result = {};

    switch (queryType) {
        case 0:
            result = await UploadModel.find();
            break;
        case 1:
            result = await UploadModel.findById(loadId);
            break;
        default:
            result = {};
            break;
    }

    const statusCode = (result == null) ? 404 : 200;
    const status = (result == null) ? "failed" : "done";

    res.status(statusCode).json({
        status : status,
        mesaage : "Results according to your query",
        result : {
            data : result
        }
    });
}

const getErrorsUpload = async (req, res) => {
    const { loadId, page, limit } = req.body;
    const resultQuery = await UploadModel.findById(loadId);
    var response = {};
    var statusCode = 200;

    if (resultQuery == null) {
        response = {
            status : "failed",
            mesaage : "No results found for the provided loadId",
            result : {
                page  : page,
                limit : limit,
                data  : {}
            }
        };

        statusCode = 404;
    } else {
        const errors = resultQuery.fileErrors;
        
        if (errors.length == 0) {
            response = {
                status : "failed",
                mesaage : "Conversion of the indicated file did not generate any errors",
                result : {
                    page  : page,
                    limit : limit,
                    data  : {}
                }
            };
        } else {
            const pageAux = parseInt(page) || 1;
            const limitAux = parseInt(limit) || 10;
            const start = (pageAux - 1) * limitAux;
    
            const pagedErros = errors.slice(start, start + limitAux);
    
            response = {
                status : "done",
                mesaage : "Errors found for the provided query",
                result : {
                    page  : page,
                    limit : limit,
                    data  : pagedErros
                }
            };
        }
    } 

    res.status(statusCode).json(response);
}

module.exports = {
    upload,
    getUpload,
    getErrorsUpload
}

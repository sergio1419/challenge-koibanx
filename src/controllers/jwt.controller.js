const jwtSvc = require("../services/jwt.service");

const getToken = (req, res) => {
    res.status(200).json({
        status : "done",
        token  : jwtSvc.generateJWT()
    });
}

module.exports = {
    getToken,
}

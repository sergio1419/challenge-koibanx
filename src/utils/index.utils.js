const fs = require('fs');

const removeFile = (path) => {
    fs.unlink(path, (err) => {
        if (err) throw { status: 500, message: err.message };
    });
}

const mapJson = (json, numberPairs, mapping) => {
    const fileErrors = [];
    const mappedJson = json.map((currentValue) => {
        try {
            const newObject = {}
            const values = Object.values(currentValue);
    
            for (let i = 0; i < numberPairs; i++) {
                const keys = Object.values(mapping[i]);;
                newObject[keys[0]] = (keys[1] == "number") ? parseFloat(values[i]) : values[i];
            }

            return newObject;
        } catch (err) {
            fileErrors.push(currentValue);
        }
    });

    return { mappedJson, fileErrors };
}

module.exports = {
    removeFile,
    mapJson
}

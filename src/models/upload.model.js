const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UploadModel = Schema({
    _id           : String,
    status        : { type : String, enum: ["pending", "processing", "done"] },
    mappingFormat : Array,
    fileErrors    : Array
}, {
    timestamps : true
});

module.exports = mongoose.model("UploadModel", UploadModel);
